// Obtenemos el button y lo almacenamos en una variable llamada "btn"
var btnSumar = document.getElementById("btnSumar");
var btnRestar = document.getElementById("btnRestar");
var btnMultiplicar = document.getElementById("btnMultiplicar");
var btnDividir = document.getElementById("btnDividir");
/* Obtenemos el div que muestra el resultado y lo
almacenamos en una variable llamada "resultado" */
var resultado = document.getElementById("resultado")
/* Obtenemos los dos input y los almacenamos en
variables "inputUno" y "inputDos" */
var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");
// Añadimos el evento click a la variable "btn"
btnSumar.addEventListener("click",function(){
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    resultado.innerHTML = suma(n1,n2);
});
btnRestar.addEventListener("click",function(){
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    resultado.innerHTML = resta(n1,n2);
});
btnMultiplicar.addEventListener("click",function(){
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    resultado.innerHTML = multiplica(n1,n2);
});
btnDividir.addEventListener("click",function(){
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    if(n2!=0){
        resultado.innerHTML = divide(n1,n2);
    }else{
        resultado.innerHTML = "<span style='color: red;'>No se puede dividir por 0</span>";
    }
});
/* Función que retorna la suma de dos números */
function suma(n1, n2){
// Es necesario aplicar parseInt a cada número
return parseInt(n1) + parseInt(n2);
}
function resta(n1, n2){
    // Es necesario aplicar parseInt a cada número
    return parseInt(n1) - parseInt(n2);
}
function multiplica(n1, n2){
    // Es necesario aplicar parseInt a cada número
    return parseInt(n1) * parseInt(n2);
}
function divide(n1, n2){
    // Es necesario aplicar parseInt a cada número
    return parseInt(n1) / parseInt(n2);
}